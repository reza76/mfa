# Mfa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Angular CLI
```commandline
npm install -g @angular/cli
ng new my-dream-app
cd my-dream-app
ng serve
```

## add bootstrap

npm i bootstrap jquery popper --save

### update angula.json with

```angular2html
"styles": [
"./node_modules/bootstrap/dist/css/bootstrap.min.css",
"src/styles.css"
],

"scripts": [
"./node_modules/bootstrap/dist/js/bootstrap.min.js",
"./node_modules/jquery/dist/jquery.min.js",
"./node_modules/popper/index.js"
]
### RESTART application
```
## ADD Angular Material
```commandline
ng add @angular/material
```
```commandline
import {MatButtonModule} from '@angular/material/button';
```

## Generate Modules
```commandline
ng generate module <module_name>
```
## Generate Component
```commandline
ng generate component <component_name>
```
## Routing Module
```commandline
ng generate module 1pp-routing --flat-module=app
```

## Lazy Loading Module
```commandline
ng g module <module_name> --route <module_route> --module app.module
ng g module payment --route payment --module app.module
```

## Route guard
```commandline
ng g guard auth
```
